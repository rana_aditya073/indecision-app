'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var IndecisionApp = function (_React$Component) {
    _inherits(IndecisionApp, _React$Component);

    function IndecisionApp(props) {
        _classCallCheck(this, IndecisionApp);

        var _this = _possibleConstructorReturn(this, (IndecisionApp.__proto__ || Object.getPrototypeOf(IndecisionApp)).call(this, props));

        _this.handleDeleteOptions = _this.handleDeleteOptions.bind(_this);
        _this.handlePick = _this.handlePick.bind(_this);
        _this.handleAddOption = _this.handleAddOption.bind(_this);
        _this.handleDeleteOption = _this.handleDeleteOption.bind(_this);
        _this.state = {
            options: []
        };
        return _this;
    }

    _createClass(IndecisionApp, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            try {
                var json = localStorage.getItem('options');
                var options = JSON.parse(json);

                if (options) this.setState(function () {
                    return { options: options };
                });
            } catch (e) {
                // do nothing at all
            }
        }
    }, {
        key: 'componentDidUpdate',
        value: function componentDidUpdate(prevProps, prevState) {

            if (prevState.options.length != this.state.options.length) {
                var json = JSON.stringify(this.state.options);
                localStorage.setItem('options', json);
                console.log('savedata!');
            }
        }
    }, {
        key: 'handleDeleteOptions',
        value: function handleDeleteOptions() {
            // this.setState(()=>{
            //     return{
            //         options:[]
            //     }
            // })

            this.setState(function () {
                return { // #2 implicit way
                    options: []
                };
            });
        }
    }, {
        key: 'handleDeleteOption',
        value: function handleDeleteOption(optionToRemove) {
            // console.log('hello ',option)
            this.setState(function (prevState) {
                return {
                    options: prevState.options.filter(function (option) {
                        return optionToRemove !== option;
                    })
                };
            });
        }
    }, {
        key: 'handlePick',
        value: function handlePick() {
            var randomnum = Math.floor(Math.random() * this.state.options.length);
            var option = this.state.options[randomnum];
            alert(option);
            // alert('handle-Pick')
        }
    }, {
        key: 'handleAddOption',
        value: function handleAddOption(option) {
            // console.log(option)
            if (!option) {
                return 'Enter valid value!';
            } else if (this.state.options.indexOf(option) > -1) {
                return 'This item already exist!';
            }

            // this.setState((prevState)=>{
            //     return{
            //         options:prevState.options.concat(option)
            //     }
            // })


            this.setState(function (prevState) {
                return { //#impicit way
                    options: prevState.options.concat(option)
                };
            });
        }
    }, {
        key: 'render',
        value: function render() {

            // const title='Indecision'
            var subtitle = 'Put your life in the hands of me :)';

            return React.createElement(
                'div',
                null,
                React.createElement(Header, { subtitle: subtitle }),
                React.createElement(Action, {
                    hasOptions: this.state.options.length > 0,
                    handlePick: this.handlePick
                }),
                React.createElement(Options, {
                    options: this.state.options,
                    handleDeleteOptions: this.handleDeleteOptions,
                    handleDeleteOption: this.handleDeleteOption
                }),
                React.createElement(Addoption, { handleAddOption: this.handleAddOption })
            );
        }
    }]);

    return IndecisionApp;
}(React.Component);

var Header = function Header(props) {
    // stateless

    return React.createElement(
        'div',
        null,
        React.createElement(
            'h1',
            null,
            props.title
        ),
        props.subtitle && React.createElement(
            'h2',
            null,
            props.subtitle
        )
    );
};

Header.defaultProps = {
    title: 'Indecision App'

    // class Header extends React.Component{

    //     render(){
    //         // return <p>This is from header</p>
    //         console.log(this.props)
    //         return (
    //             <div>

    //             <h1>{this.props.title}</h1>
    //             <h2>{this.props.subtitle}</h2>
    //             </div>
    //         )
    //     }

    // }


};var Options = function Options(props) {
    // stateless
    return React.createElement(
        'div',
        null,
        React.createElement(
            'button',
            { onClick: props.handleDeleteOptions },
            'Remove All'
        ),
        props.options.length === 0 && React.createElement(
            'p',
            null,
            'Please add options to set up!'
        ),
        props.options.map(function (items) {
            return React.createElement(Option, {
                key: items,
                text: items,
                handleDeleteOption: props.handleDeleteOption
            });
        })
    );
};

// class Options extends React.Component{

//     // constructor(props){
//     //     super(props)
//     //     this.removeAll=this.removeAll.bind(this)
//     // }

//     // removeAll(){
//     //     console.log(this.props.options)
//     //     // alert('removed')
//     // }

//     render(){
//         return (
//             <div>
//             <button onClick={this.props.handleDeleteOptions}>Remove All</button>
//             {
//                 this.props.options.map((items)=>{
//                     return <Option key={items} text={items}/>})
//             }
//             <Option/>

//             </div>
//         )
//     }
// }


var Option = function Option(props) {
    return React.createElement(
        'div',
        null,
        props.text,
        React.createElement(
            'button',
            {
                onClick: function onClick(e) {
                    props.handleDeleteOption(props.text);
                }
            },
            'Remove'
        )
    );
};

// class Option extends React.Component{
//     render(){
//         return (
//            <div>
//            {this.props.text}

//            </div>
//         )
//     }
// }

var Action = function Action(props) {
    // stateless function components
    return React.createElement(
        'div',
        null,
        React.createElement(
            'button',
            { disabled: !props.hasOptions, onClick: props.handlePick },
            'What do you wanna do?'
        )
    );
};

// class Action extends React.Component{
//     // handlePick(){
//     //     alert('handlepick')
//     // }

//     render(){
//         return (
//             <div>
//             <button disabled={!this.props.hasOptions} onClick={this.props.handlePick}>
//             What do you wanna do?</button>
//             </div>
//         )
//     }
// }

var Addoption = function (_React$Component2) {
    _inherits(Addoption, _React$Component2);

    function Addoption(props) {
        _classCallCheck(this, Addoption);

        var _this2 = _possibleConstructorReturn(this, (Addoption.__proto__ || Object.getPrototypeOf(Addoption)).call(this, props));

        _this2.handleAddOption = _this2.handleAddOption.bind(_this2);
        _this2.state = {
            error: undefined
        };
        return _this2;
    }

    _createClass(Addoption, [{
        key: 'handleAddOption',
        value: function handleAddOption(e) {
            e.preventDefault();
            var option = e.target.elements.option.value.trim();
            var error = this.props.handleAddOption(option);

            // this.setState(()=>{
            //     return{
            //         error
            //     }
            // })

            this.setState(function () {
                return { // #implicit way
                    error: error
                };
            });
            if (!error) {
                e.target.elements.option.value = '';
            }

            document.chatform.reset();
        }
    }, {
        key: 'render',
        value: function render() {
            return React.createElement(
                'div',
                null,
                this.state.error && React.createElement(
                    'p',
                    null,
                    alert(this.state.error)
                ),
                React.createElement(
                    'form',
                    { name: 'chatform', id: 'chatform', onSubmit: this.handleAddOption },
                    React.createElement('input', { type: 'text', placeholder: 'enter items', name: 'option' }),
                    React.createElement(
                        'button',
                        null,
                        'Add option'
                    )
                )
            );
        }
    }]);

    return Addoption;
}(React.Component);

// const User= (props)=>{
//     return (
//         <div>
//         <p>Name: {props.name}</p>
//         <p>Age: {props.age}</p>
//         </div>
//     )
// } 


ReactDOM.render(React.createElement(IndecisionApp, null), document.getElementById('app'));

// const jsx=(
//     <div>
//     <h1>Title</h1>
//     <Header />
//     <Option />
//     <Action />
//     <Addoption />
//     </div>
// )
// ReactDOM.render(jsx,document.getElementById('app'))
