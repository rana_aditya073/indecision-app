class Counter extends React.Component{
    constructor(props){
        super(props)
        this.addOne=this.addOne.bind(this)
        this.minusOne=this.minusOne.bind(this)
        this.reset=this.reset.bind(this)
        this.state={
            // count:props.count
            count:0
        }
    }
    addOne(){
      this.setState((prevState)=>{
          return{
              count:prevState.count+1
          }
      })
    }
    minusOne(){
        // console.log('-1')
        this.setState((prevState)=>{
            return{
                count:prevState.count-1
            }
        })
    }
    reset(){
        console.log('reset')
        this.setState(()=>{
            return{
                count:0
            }
        })
     
    }

    componentDidMount(){
        const json=localStorage.getItem('count')
        const count=JSON.parse(json)

        if(!isNaN(count))
        this.setState(()=>({count}))
    }
    componentDidUpdate( prevProps,prevState){
        if(prevState.count!==this.state.count)
        {
            // const json=JSON.stringify(this.state.count)
        localStorage.setItem('count',this.state.count);
        console.log('saved!')
        }
    }
    render(){
        return (
            <div>
            <h1>Count: {this.state.count}</h1>
            <button onClick={this.addOne}>+1</button>
            <button onClick={this.minusOne}>-1</button>
            <button onClick={this.reset}>reset</button>
            </div>
        )
    }
}



ReactDOM.render(<Counter/>, document.getElementById('app'))


// let count=0
// // const someid='myid'
// const addone=()=>{
//     count++
//     rendercounter()
// }
// const minusone=()=>{
//     count--
//     rendercounter()
// }
// const reset=()=>{
//     count=0
//     rendercounter()
// }



// const rendercounter=()=>{
//     const template2 =(
//         <div>
//         <h1>Count:{count}</h1>
//         <button onClick={addone}>+1</button>
//         <button onClick={minusone}>-1</button>
//         <button onClick={reset}>reset</button>
//         </div>
    
//     )
//     ReactDOM.render(template2, id); 

// }

// const id=document.getElementById('app')

// rendercounter()