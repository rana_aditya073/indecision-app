class Person{

constructor(name='Anonymous',age=0){
this.name=name
this.age=age
}

sayHello(){
    // return 'Hi '+this.name +' How are you there :P'
    return `Hi there ${this.name} How are u there! `
}
tellAge(){
    return `Wow you are ${this.age} years old`
}

getdescription(){
    return `Hello ${this.name} Age: ${this.age} `
}

}

class Student extends Person{
    constructor(name, age, major="undecided"){
        super(name, age)
        this.major=major
    }

    hasMajor(){
        return this.major!=='undecided'
    }

    getdescription(){
        let description= super.getdescription()
        if(this.hasMajor())
        return description+=` ${this.major}`
    

    return description
    }
}

class Traveller extends Person{
    constructor(name, age,home="undecided"){
        super(name,age)
        this.home=home

    }

    haslocation(){
        return this.home!=='undecided'
    }

    getGreeting(){
        let greeting = super.getdescription()
        if(this.haslocation()){
            return greeting+= ` I'm from ${this.home}`
        }

        return `I'm from ${this.home}`
    }
}

const me= new Student('Aditya',23,'Comp Science')
console.log(me)
console.log(me.hasMajor())

console.log(me.getdescription())

const other=new Student()
console.log(other)
console.log(other.getdescription())
console.log(other.hasMajor())

const traveller= new Traveller('Aditya',23,'Delhi')
console.log(traveller)
console.log(traveller.getGreeting())
console.log(traveller.haslocation())