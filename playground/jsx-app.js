

class IndecisionApp extends React.Component {

    constructor(props) {
        super(props)
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this)
        this.handlePick = this.handlePick.bind(this)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.handleDeleteOption = this.handleDeleteOption.bind(this)
        this.state = {
            options: []
        }
    }
    componentDidMount(){
        try{
        const json=localStorage.getItem('options')
        const options=JSON.parse(json)

        if(options)
        this.setState(()=>({ options}))
        }catch(e){
            // do nothing at all
        }
    }
    componentDidUpdate(prevProps, prevState){

        if(prevState.options.length!=this.state.options.length){
            const json= JSON.stringify(this.state.options)
            localStorage.setItem('options',json);            
            console.log('savedata!')    
        }
        
    }
    
    handleDeleteOptions() {
        // this.setState(()=>{
        //     return{
        //         options:[]
        //     }
        // })

        this.setState(() => ({   // #2 implicit way
            options: []
        }))
    }

    handleDeleteOption(optionToRemove) {
        // console.log('hello ',option)
        this.setState((prevState)=>({
            options:prevState.options.filter((option)=>{
                return optionToRemove !==option;
            })
        }))
    }


    handlePick() {
        const randomnum = Math.floor(Math.random() * this.state.options.length)
        const option = this.state.options[randomnum]
        alert(option)
        // alert('handle-Pick')
    }

    handleAddOption(option) {
        // console.log(option)
        if (!option) {
            return 'Enter valid value!'
        }
        else if (this.state.options.indexOf(option) > -1) {
            return 'This item already exist!'
        }

        // this.setState((prevState)=>{
        //     return{
        //         options:prevState.options.concat(option)
        //     }
        // })


        this.setState((prevState) => ({  //#impicit way
            options: prevState.options.concat(option)
        }))
    }


    render() {

        // const title='Indecision'
        const subtitle = 'Put your life in the hands of me :)'


        return (
            <div>
                <Header  subtitle={subtitle} />
                <Action
                    hasOptions={this.state.options.length > 0}
                    handlePick={this.handlePick}
                />
                <Options
                    options={this.state.options}
                    handleDeleteOptions={this.handleDeleteOptions}
                    handleDeleteOption={this.handleDeleteOption}
                />
                <Addoption handleAddOption={this.handleAddOption} />

            </div>
        )
    }
}

const Header = (props) => { // stateless

    return (
        <div>

            <h1>{props.title}</h1>
            {props.subtitle && <h2>{props.subtitle}</h2>}
        </div>
    )
}

Header.defaultProps = {
    title: 'Indecision App'
}

// class Header extends React.Component{

//     render(){
//         // return <p>This is from header</p>
//         console.log(this.props)
//         return (
//             <div>

//             <h1>{this.props.title}</h1>
//             <h2>{this.props.subtitle}</h2>
//             </div>
//         )
//     }

// }


const Options = (props) => { // stateless
    return (
        <div>
            <button onClick={props.handleDeleteOptions}>Remove All</button>
            {props.options.length===0 && <p>Please add options to set up!</p>}
            {
                props.options.map((items) => (
                    <Option
                        key={items}
                        text={items}
                        handleDeleteOption={props.handleDeleteOption}
                    />

                ))
            }

        </div>
    )
}

// class Options extends React.Component{

//     // constructor(props){
//     //     super(props)
//     //     this.removeAll=this.removeAll.bind(this)
//     // }

//     // removeAll(){
//     //     console.log(this.props.options)
//     //     // alert('removed')
//     // }

//     render(){
//         return (
//             <div>
//             <button onClick={this.props.handleDeleteOptions}>Remove All</button>
//             {
//                 this.props.options.map((items)=>{
//                     return <Option key={items} text={items}/>})
//             }
//             <Option/>

//             </div>
//         )
//     }
// }


const Option = (props) => {
    return (
        <div>
            {props.text}
            <button
                onClick={(e) => {
                    props.handleDeleteOption(props.text)
                }}
            >
                Remove</button>
        </div>
    )
}

// class Option extends React.Component{
//     render(){
//         return (
//            <div>
//            {this.props.text}

//            </div>
//         )
//     }
// }

const Action = (props) => {  // stateless function components
    return (
        <div>
            <button disabled={!props.hasOptions} onClick={props.handlePick}>
                What do you wanna do?
        </button>
        </div>
    )
}


// class Action extends React.Component{
//     // handlePick(){
//     //     alert('handlepick')
//     // }

//     render(){
//         return (
//             <div>
//             <button disabled={!this.props.hasOptions} onClick={this.props.handlePick}>
//             What do you wanna do?</button>
//             </div>
//         )
//     }
// }

class Addoption extends React.Component {

    constructor(props) {
        super(props)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.state = {
            error: undefined
        }
    }

    handleAddOption(e) {
        e.preventDefault()
        const option = e.target.elements.option.value.trim()
        const error = this.props.handleAddOption(option)

        // this.setState(()=>{
        //     return{
        //         error
        //     }
        // })

        this.setState(() => ({  // #implicit way
            error
        }))
        if(!error){
            e.target.elements.option.value=''
        }


        document.chatform.reset()
    }

    render() {
        return (
            <div>
                {this.state.error && <p>{alert(this.state.error)}</p>}
                <form name="chatform" id="chatform" onSubmit={this.handleAddOption}>
                    <input type='text' placeholder='enter items' name='option'></input>
                    <button>Add option</button>
                </form>

            </div>
        )
    }
}

// const User= (props)=>{
//     return (
//         <div>
//         <p>Name: {props.name}</p>
//         <p>Age: {props.age}</p>
//         </div>
//     )
// } 


ReactDOM.render(<IndecisionApp />, document.getElementById('app'))

// const jsx=(
//     <div>
//     <h1>Title</h1>
//     <Header />
//     <Option />
//     <Action />
//     <Addoption />
//     </div>
// )
// ReactDOM.render(jsx,document.getElementById('app'))