console.log('app.js running')
//JSX
// const template = <p>This is JSX!</p>;// will throw error see babeljs.io
// const template= React.createElement("p",null,"Hello This is my JSX");


const user = {
    name: '',
    age: 23,
    location: 'London'
}

const getlocation = (location) => {
    if (location)
        return <h3>
            City:{location}
        </h3>

    return undefined;
}

console.log(getlocation('paris'))

const app = {
    title: 'Indecision-App',
    subtitle: 'Hey There',
    options: ['one', 'two']
}

const getsubtitle = (subtitle) => {
    if (subtitle) {
        return <h2>{app.subtitle}</h2>
    }
    return undefined
}

const onformSubmit = (e) => {
    e.preventDefault()
    // console.log('submitted')
    const option = e.target.elements.option.value
    if (option) {
        app.options.push(option)
        e.target.elements.option.value = ''
        renderform()
    }
}

const onclickbtn = (e) => {
    e.preventDefault()
    app.options = []
    renderform()
}

const onmakeDecision = () => {
    const randomnum = Math.floor(Math.random() * app.options.length)
    const option = app.options[randomnum]
    // console.log(randomnum)
    alert(option)
}

let Visibility = false
const toggleme = () => {
    Visibility = !Visibility
    console.log(Visibility)
    renderform()

}


const id = document.getElementById('app');
const numbers = [12, 14, 15]

const renderform = () => {
    const template1 = (
        <div>
            <h1>{app.title}</h1>
            {getsubtitle(app.subtitle)}



            {(app.options && app.options.length > 0) ?
                'Here are your options'

                : 'There are none :/'}

            <button disabled={app.options.length === 0} onClick={onmakeDecision}>What Shud I do?</button>
            <ol>
                {
                    app.options.map((items) => {
                        return <li key={items}>{items}</li>
                    })
                }
            </ol>

            <form onSubmit={onformSubmit}>
                <input type="text" name="option" placeholder="text"></input>
                <button>Add option</button>
            </form>
            <button onClick={onclickbtn}>Remove All</button>
        </div>
    )

    const template2 = (
        <div>
            <h1>Visibility Toggle</h1>
            <button onClick={toggleme}>
                {Visibility ? 'Show Details' : 'Hide Details'}
            </button>
            {Visibility?'':
            <div>
            <p>Hello there how are u doing!</p>
            </div>}

        </div>


    )

    const template3=(
        <div>
        <p>
        <h3>
        Hello World!
        </h3>
        </p>
        </div>
    )




    ReactDOM.render(template3, id)

}

renderform()