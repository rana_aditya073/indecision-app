import React from 'react'
import Option from './Option'
const Options = (props) => { // stateless
    return (
        <div>
            <div className="widget-header">
                <h3>Your Options</h3>
                <button
                    className="remove-button"
                    onClick={props.handleDeleteOptions}>
                    Remove All
            </button>
            </div>
            {props.options.length === 0 &&
                <p className="_option">
                    Please add options to set up!</p>}

            
                {
                    props.options.map((items,index) => (

                        <Option
                            key={items}
                            text={items}
                            count={index+1}
                            handleDeleteOption={props.handleDeleteOption}
                        />


                    ))
                }
            


        </div>
    )
}

export default Options

//or simply export default and remove const Options =