import React from 'react'


const Action = (props) => {  // stateless function components
    return (
        <div >
            <button 
            className="big-button"
            disabled={!props.hasOptions} onClick={props.handlePick}>
                What do you wanna do?
        </button>
        </div>
    )
}

export default Action