import React from 'react'
import Modal from 'react-modal'

const OptionModal=(props)=>{
return(
 <Modal
 isOpen={!!props.selectedOption}
 contentLabel="Selected Option"
 onRequestClose={props.handleRemove}
 closeTimeoutMS={200}
 className="modal"
 >
 <h3 className="modal_title">Selected Option</h3>
 {props.selectedOption && <p className="modal_body">{props.selectedOption}</p>}
 <button className="remove-button" onClick={props.handleRemove}>Close</button>
 </Modal>
)
}

export default OptionModal;
