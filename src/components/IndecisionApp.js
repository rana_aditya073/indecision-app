import React from 'react'
import Addoption from './AddOption'
import Options from './Options'
import Option from './Option'
import Header from './Header'
import Action from './Action'
import OptionModal from './OptionModal'
class IndecisionApp extends React.Component {

    constructor(props) {
        super(props)
        this.handleDeleteOptions = this.handleDeleteOptions.bind(this)
        this.handlePick = this.handlePick.bind(this)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.handleDeleteOption = this.handleDeleteOption.bind(this)
        this.handleRemove=this.handleRemove.bind(this)
        this.state = {
            options: [],
            selectedOption:undefined
        }
    }
    componentDidMount(){
        try{
        const json=localStorage.getItem('options')
        const options=JSON.parse(json)

        if(options)
        this.setState(()=>({ options}))
        }catch(e){
            // do nothing at all
        }
    }
    componentDidUpdate(prevProps, prevState){

        if(prevState.options.length!=this.state.options.length){
            const json= JSON.stringify(this.state.options)
            localStorage.setItem('options',json);            
            console.log('savedata!')    
        }
        
    }
    
    handleDeleteOptions() {
        // this.setState(()=>{
        //     return{
        //         options:[]
        //     }
        // })

        this.setState(() => ({   // #2 implicit way
            options: []
        }))
    }

    handleDeleteOption(optionToRemove) {
        // console.log('hello ',option)
        this.setState((prevState)=>({
            options:prevState.options.filter((option)=>{
                return optionToRemove !==option;
            })
        }))
    }


    handlePick() {
        const randomnum = Math.floor(Math.random() * this.state.options.length)
        const option = this.state.options[randomnum]
        this.setState(()=>({
            selectedOption:option
        }))
        // alert(option)
        // alert('handle-Pick')
    }

    handleRemove(){
        this.setState(()=>({
            selectedOption:undefined
        }))
    }
    handleAddOption(option) {
        // console.log(option)
        if (!option) {
            return 'Enter valid value!'
        }
        else if (this.state.options.indexOf(option) > -1) {
            return 'This item already exist!'
        }

        // this.setState((prevState)=>{
        //     return{
        //         options:prevState.options.concat(option)
        //     }
        // })


        this.setState((prevState) => ({  //#impicit way
            options: prevState.options.concat(option)
        }))
    }


    render() {

        // const title='Indecision'
        const subtitle = 'Put your life in the hands of me :)'


        return (
            <div >
                <Header  subtitle={subtitle} />
                <div className="container">
                <Action
                    hasOptions={this.state.options.length > 0}
                    handlePick={this.handlePick}
                />
                <div className="widget">
                <Options
                    options={this.state.options}
                    handleDeleteOptions={this.handleDeleteOptions}
                    handleDeleteOption={this.handleDeleteOption}
                />
                
                <Addoption handleAddOption={this.handleAddOption} />
                
                </div>
                <OptionModal
                selectedOption={this.state.selectedOption}
                handleRemove={this.handleRemove}/>
                </div>
            </div>
        )
    }
}
export default IndecisionApp