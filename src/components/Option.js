import React from 'react' 

export default (props) => {
    return (
        <div className="option">
            <p
            className="items"
            >{props.count}. {props.text}</p>
            
            <button
            className="button remove-button"
                onClick={(e) => {
                    props.handleDeleteOption(props.text)
                }}
            >
                Remove</button>
        </div>
    )
}
