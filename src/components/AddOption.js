import React  from  'react'


class Addoption extends React.Component {

    constructor(props) {
        super(props)
        this.handleAddOption = this.handleAddOption.bind(this)
        this.state = {
            error: undefined
        }
    }

    handleAddOption(e) {
        e.preventDefault()
        const option = e.target.elements.option.value.trim()
        const error = this.props.handleAddOption(option)

        // this.setState(()=>{
        //     return{
        //         error
        //     }
        // })

        this.setState(() => ({  // #implicit way
            error
        }))
        if(!error){
            e.target.elements.option.value=''
        }


        document.chatform.reset()
    }

    render() {
        return (
            <div>
                {this.state.error && <p>{alert(this.state.error)}</p>}
                
                <form 
                className="forms"
                name="chatform" id="chatform" onSubmit={this.handleAddOption}
                >
                    <input type='text' placeholder='Enter items..' name='option'></input>
                    <button className="add-button">
                    Add option</button>
                </form>
                </div>

            
        )
    }
}


export default Addoption